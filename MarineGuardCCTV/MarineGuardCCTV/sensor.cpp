/*
* sensor.cpp
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#define _IS_ON_RASPI
#include <wiringPi.h>


#include "sensor.h"
#include "network.h"

bool HumanSensor::getIsSensorOn()
{
#ifdef _IS_ON_RASPI
	// Actual running on Raspberry PI

	if (wiringPiSetup() == -1)
	{
		(*Log::getInstance())("Error", "HumanSensor::getIsSensorOn()::wiringPiSetup() failed.");
		printf("Error: wiringPiSetup() failed.\n");
		exit(-1);
	}
	//printf("[Test]PIN %d read.\n",_raspi_pin);
	pinMode(_raspi_pin, INPUT);
	int sw_val = digitalRead(_raspi_pin);
	//(*Log::getInstance())("Sensor","digitalRead();");
	if (sw_val == 1) 
	{
		//(*Log::getInstance())("Sensor","digitalRead()==FALSE;");
		return false;
	}
	//(*Log::getInstance())("Sensor","digitalRead()==TRUE;");
	return true;

#else
	// Dummy sensor for PC Tests
	(*Log::getInstance())("Sensor","Dummy sensor starts.");
	static int num = 0;
	num++;
	if (num % 60 < 30)
	{
		return true;
	}
	return false;
#endif
}

void HumanSensor::sensorLoopFunc()
{
	bool curStatus = getIsSensorOn();
	if (_last_status == false && curStatus == true)
	{
		(*Log::getInstance())("HumanSensor","Sensor detected human in specified area!");
		// RTSP on
		string rtspCommand = Settings::getInstance()->rtspCmdLine();
		(*Log::getInstance())("RTSP On", ("Command Line: " + rtspCommand).c_str());
#ifndef _MSC_VER
		system(rtspCommand.c_str());
#endif
		// Socket:: TCP/UDP packet -> Android
		for (auto it : Settings::getInstance()->hostnames())
		{
			// TODO: Socket
			(*Log::getInstance())("Socket", ("Sending to " + it).c_str());

			Network::getInstance()->sendMessage(it, "Alert!");
		}
		if (Settings::getInstance()->isAlarmOn())
		{
			Audio::getInstance()->playSound(
				Settings::getInstance()->alarmSoundName(), 
				Settings::getInstance()->isAlarmLoop());
		}
	}
	_last_status = curStatus;
}

const std::string HumanSensor::getSensorInfo() const
{
	return std::string("IR Human Sensor");
}
