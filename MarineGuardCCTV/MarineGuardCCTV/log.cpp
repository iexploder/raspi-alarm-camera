/*
* log.cpp
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#include "log.h"
#include <ctime>
#include <memory.h>

Log* Log::_pInstance = nullptr;
Log* Log::getInstance()
{
	if (_pInstance == nullptr)_pInstance = new Log;
	return _pInstance;
}

void Log::operator()(const char* title, const char* msg)
{
	// Get current time
	time_t curTime = time(nullptr);
	tm* now = localtime(&curTime);
	// Make it string like 
	char curTimeStr[] = "9102-99-99 12:60:60 ";
	sprintf(curTimeStr, "%04d-%02d-%02d %02d:%02d:%02d", 
		now->tm_year + 1900,
		now->tm_mon + 1,
		now->tm_mday,
		now->tm_hour,
		now->tm_min,
		now->tm_sec);

	// Log -> file
	FILE* fp = nullptr;
	fp = fopen(_strLogFileName.c_str(), "a+");
	if (!fp)
	{
		printf("[%s]%s\n", "Fatal ERROR", "kak::Log open file failure.");
		exit(-1);
	}
	fprintf(fp, "%s [%s] %s\n", curTimeStr, title, msg);
	fclose(fp);

	// Log -> Print
	printf ("%s [%s] %s\n", curTimeStr, title, msg);

	// Log -> vector
	char _logBuffer[Message_MaxSize];
	memset(_logBuffer, 0, Message_MaxSize);
	sprintf(_logBuffer, "%s [%s] %s", curTimeStr, title, msg);

	// Avoid issues when messages exceed MaxSize
	_logBuffer[Message_MaxSize - 1] = 0;
	_logData.emplace_back(_logBuffer);
}

void Log::print()
{
	for (auto it : _logData)
	{
		printf("%s\n", it.c_str());
	}
}

