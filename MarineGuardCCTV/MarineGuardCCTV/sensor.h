/*
* sensor.h
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#pragma once

#include <functional>
#include <string>

#include "settings.h"
#include "log.h"
#include "audio.h"

class Sensor
{
public:
	// ABIs
	virtual void sensorLoopFunc() = 0;
	virtual bool getIsSensorOn() = 0;
	virtual const std::string getSensorInfo() const = 0;
	virtual ~Sensor() { /* Do Nothing Here */ }
};

class HumanSensor : public Sensor
{
private:
	int _raspi_pin = 12;

	bool _last_status = false;
public:
	HumanSensor(int raspi_pin) : _raspi_pin(raspi_pin) { /* Do Nothing here */ }
	virtual bool getIsSensorOn();
	virtual void sensorLoopFunc();
	virtual const std::string getSensorInfo() const;
};
