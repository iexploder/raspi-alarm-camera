#pragma once

/*
* settings.h
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#include "log.h"

// JSON parser (header-only lib)
#include "picojson.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using std::string;
using std::vector;
using std::cout;
using std::ifstream;

class Settings
{
public:
	static Settings* getInstance();
private:
	static Settings* _pInstance;

private:
	string _strSettingsJsonRaw;


	// When the sensor(s) detect person, alarm RING or NOT?
	bool _isAlarmOn = true;
	string _isAlarmOnJson = "alarm";

	// Alarm Sound file name.
	string _alarmSoundName = "/home/pi/Music/hyoushigi1.mp3";
	string _alarmSoundNameJson = "alarm_sound";

	// Alarm ring loop or not
	bool _isAlarmLoop = false;
	string _isAlarmLoopJson = "alarm_loop";

	// Target hostname(s) notifications would be sent to
	vector<string> _hostnames = { "192.168.11.159:8080" };
	string _hostnamesJson = "hostnames";

	// External Webcam RTSP module invocation
	string _rtspCmdLine = "sudo systemctl start v412_rtspserver.service";
	string _rtspCmdLineJson = "rtsp_cmdline";

public:
	bool readFromFile(const string& fileName);
	bool apply();
	string output();

	bool isAlarmOn() const { return _isAlarmOn; }
	bool isAlarmLoop() const { return _isAlarmLoop; }
	const string alarmSoundName() const { return _alarmSoundName; }
	const vector<string> hostnames() const { return _hostnames; }
	const string rtspCmdLine() const { return _rtspCmdLine; }

};

