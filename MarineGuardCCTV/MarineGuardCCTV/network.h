/*
* network.h
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/
#pragma once

#include <string>
#include <sstream>
#include <cstdint>
#include <memory>

#include "easywsclient.hpp"


#include "log.h"

using std::string;
using std::stringstream;
using easywsclient::WebSocket;

class Network
{
	// Singleton Trick
private:
	static Network* _pInstance;
public:
	static Network* getInstance();
	
public:
	bool sendMessage(const string& hostname, const string& msg);

};