#include "settings.h"
#include "sensor.h"

#ifdef _MSC_VER
#include <Windows.h>
#else
#include <unistd.h>
#endif

int main()
{
	Settings::getInstance()->readFromFile("config.json");
	Settings::getInstance()->apply();
	std::cout << Settings::getInstance()->output() << "\n";
	vector<Sensor*> vecSensor;
	vecSensor.emplace_back(new HumanSensor(12));
	for (;;)
	{
#ifdef _MSC_VER
		Sleep(10);
#else
		sleep(1);
#endif
		for(auto it : vecSensor)
		{
			it->sensorLoopFunc();
		}
	}
	return 0;
}

