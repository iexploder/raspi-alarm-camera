#pragma once
/*
* log.h
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/
#include <cstdio>

#include <string>
#include <vector>
using std::string;
using std::vector;

constexpr size_t Message_MaxSize = 2048;

class Log
{
public:
	static Log* getInstance();
private:
	static Log* _pInstance;
	Log(){/* Prevent initialization outside. */}

private:
	string _strLogFileName = "marineguardcctv.log";
	vector<string> _logData;
public:
	// Log a line.
	void operator()(const char* title, const char* msg);

	// Output to cmdline
	void print();
};
