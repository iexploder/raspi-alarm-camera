#include "settings.h"

/*
* settings.cpp
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/


// Singleton static pointer initialize
Settings* Settings::_pInstance = nullptr;
Settings* Settings::getInstance()
{
	if (_pInstance == nullptr) _pInstance = new Settings;
	return _pInstance;
}

bool Settings::readFromFile(const string & fileName)
{
	string strFileData;
	ifstream ifs(fileName, ifstream::in);
	string strBuffer;
	while (!ifs.eof())
	{
		std::getline(ifs, strBuffer);
		strFileData += strBuffer;
	}

	_strSettingsJsonRaw = strFileData;

	return true;
}
bool Settings::apply()
{
	picojson::value _value;
	string err = picojson::parse(_value, _strSettingsJsonRaw);
	if (!err.empty())
	{

		err = (string)"kak::Settings::apply() : " + err;

		// Print out
		std::cerr << "[Error]" << err << std::endl;
		// Log
		(*Log::getInstance())("Error", err.c_str());
		return false;
	}


	// Parse
	picojson::value _valBuffer;
	string log_content;

	if (_value.is<picojson::object>())
	{
		// isAlarmOn? : bool
		_valBuffer = _value.get(_isAlarmOnJson);
		log_content = _isAlarmOnJson + " is set to ";

		if (_valBuffer.is<bool>())
		{
			_isAlarmOn = _valBuffer.get<bool>();

			if (_isAlarmOn)log_content += "TRUE.";
			else log_content += "FALSE.";

			(*Log::getInstance())("Settings", log_content.c_str());
		}

		// alarmSoundName : string
		_valBuffer = _value.get(_alarmSoundNameJson);
		log_content = _alarmSoundNameJson + " is set to ";
		if (_valBuffer.is<string>())
		{
			_alarmSoundName = _valBuffer.get<string>();
			log_content += _alarmSoundName;

			(*Log::getInstance())("Settings", log_content.c_str());
		}

		// isAlarmLoop? : bool
		_valBuffer = _value.get(_isAlarmLoopJson);
		log_content = _isAlarmLoopJson + " is set to ";
		if (_valBuffer.is<bool>())
		{
			_isAlarmLoop = _valBuffer.get<bool>();

			if (_isAlarmLoop)log_content += "TRUE.";
			else log_content += "FALSE.";

			(*Log::getInstance())("Settings", log_content.c_str());
		}

		// hostnames : string array
		_hostnames.clear();

		_valBuffer = _value.get(_hostnamesJson);
		log_content = _hostnamesJson + " is set to {";
		if (_valBuffer.is<picojson::array>())
		{
			for (auto it : _valBuffer.get<picojson::array>())
			{
				if (it.is<string>())
				{
					_hostnames.emplace_back(it.get<string>());
					log_content += "\"" + it.get<string>() + "\", ";
				}
			}
			log_content += "}.";

			(*Log::getInstance())("Settings", log_content.c_str());
		}

		// rtspCmdLine : string
		_valBuffer = _value.get(_rtspCmdLineJson);
		log_content = _rtspCmdLineJson + " is set to ";
		if (_valBuffer.is<string>())
		{
			_rtspCmdLine = _valBuffer.get<string>();
			log_content += _rtspCmdLine;

			(*Log::getInstance())("Settings", log_content.c_str());
		}
		return true;
	}

	// Case : Unexpected format
	log_content = "kak::Settings::apply(): Settings file with unexpected format.";
	(*Log::getInstance())("Error", log_content.c_str());

	return false;
}
string Settings::output()
{
	using picojson::object;
	using picojson::value;
	using picojson::array;

	object obj;

	// Use (container.)emplace function (C++ 11~ feature) to 
	// avoid unnecessary overhead on constructor(s).
	obj.emplace(_isAlarmOnJson, _isAlarmOn);
	obj.emplace(_alarmSoundNameJson, _alarmSoundName);
	obj.emplace(_isAlarmLoopJson, _isAlarmLoop);

	array arrHostname;
	for (auto it : _hostnames)
	{
		arrHostname.emplace_back(value(it));
	}
	obj.emplace(_hostnamesJson, arrHostname);
	obj.emplace(_rtspCmdLineJson, _rtspCmdLine);


	return value(obj).serialize(true);

}
