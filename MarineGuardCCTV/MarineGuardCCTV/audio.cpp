/*
* audio.cpp
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#include "audio.h"

Audio* Audio::_pInstance = nullptr;

Audio* Audio::getInstance()
{
	if (_pInstance == nullptr)
	{
		_pInstance = new Audio;
	}
	return _pInstance;
}

void Audio::playSound(string fileName, bool loop)
{
	// Generate command line in string
	string cmdLine = _audioPlayCmdLine + " ";
	if (loop) cmdLine += _audioPlayOptionLoop + " ";
	cmdLine += fileName;

	
	system(cmdLine.c_str());

	(*Log::getInstance())("Audio", ("Command: " + cmdLine).c_str());

}
