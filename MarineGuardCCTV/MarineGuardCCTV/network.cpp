#include "network.h"
#include "easywsclient.cpp"

Network* Network::_pInstance = nullptr;

Network* Network::getInstance()
{
	if (_pInstance == nullptr) _pInstance = new Network;
	return _pInstance;
}

bool Network::sendMessage(const string& hostname, const string& msg)
{
	WebSocket::pointer ws = WebSocket::from_url("ws://" + hostname);
	if (!ws)
	{
		(*Log::getInstance())("Network", "WebSocket init error.");
		return false;
	}
	ws->poll();
	ws->send(msg);
	ws->poll();
	ws->close();
	(*Log::getInstance())("Network", ("Send Message: " + msg + " to " + hostname).c_str());

	delete ws;
	return true;
}


