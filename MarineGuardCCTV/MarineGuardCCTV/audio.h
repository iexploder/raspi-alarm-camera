#pragma once
/*
* audio.h
* ---------------------------------------------------
* Copyright 2019 Team Kaki-Aji-Komori
* QI Quanyu (aka. iExploder)
*/

#include <string>
#include <cstdlib>

#include "log.h"

using std::string;

class Audio
{
	// Singleton tricks
public:
	static Audio* getInstance();
private:
	static Audio* _pInstance;

private:
	const string _audioPlayCmdLine = "omxplayer";
	const string _audioPlayOptionLoop = "--loop";

public:
	void playSound(string fileName, bool loop = false);
};

