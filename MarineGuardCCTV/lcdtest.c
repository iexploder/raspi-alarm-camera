#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

int main()
{
	int dev_fd;
	int data;
	dev_fd = wiringPiI2CSetup(0x3e);
	if (dev_fd < 0)
	{
		printf("Error!\n");
		return 0;
	}
	
	wiringPiI2CWrite(dev_fd,0b00111000);
	usleep(5000);
	wiringPiI2CWrite(dev_fd,0b00111001);
	usleep(5000);
	wiringPiI2CWrite(dev_fd,0b00010100);
	usleep(5000);
	wiringPiI2CWrite(dev_fd,0b01110000);
	usleep(5000);
	wiringPiI2CWriteReg8(dev_fd,0x00,'A');	
	usleep(50000);
	
	wiringPiI2CWrite(dev_fd, 0x22);

	return 0;
}
